//
//  ViewController.swift
//  PDFAudio - PDF to Audiobook
//
//  Created by Arjun Srivastava on 4/29/17.
//  Copyright © 2017 Biscuit and Chai. All rights reserved.
//

import UIKit
import AVFoundation
import TesseractOCR

class ViewController: UIViewController, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, G8TesseractDelegate {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let tesseract = G8Tesseract(language: "eng", engineMode: .tesseractCubeCombined)!
    
    let synth = AVSpeechSynthesizer()
    var utter = AVSpeechUtterance(string: "")
    var startedSpeaking = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tesseract.delegate = self
    }
    

    @IBAction func takePhoto(sender: AnyObject) {
        let imagePickerActionSheet = UIAlertController(title: "Snap/Upload Photo",
                                                       message: nil, preferredStyle: .actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraButton = UIAlertAction(title: "Take Photo",
                                             style: .default) { (alert) -> Void in
                                                let imagePicker = UIImagePickerController()
                                                imagePicker.delegate = self
                                                imagePicker.sourceType = .camera
                                                self.present(imagePicker,
                                                                           animated: true,
                                                                           completion: nil)
            }
            imagePickerActionSheet.addAction(cameraButton)
        }

        let libraryButton = UIAlertAction(title: "Choose Existing",
                                          style: .default) { (alert) -> Void in
                                            let imagePicker = UIImagePickerController()
                                            imagePicker.delegate = self
                                            imagePicker.sourceType = .photoLibrary
                                            self.present(imagePicker,
                                                                       animated: true,
                                                                       completion: nil)
        }
        imagePickerActionSheet.addAction(libraryButton)
        
        let cancelButton = UIAlertAction(title: "Cancel",
                                         style: .cancel) { (alert) -> Void in
        }

        imagePickerActionSheet.addAction(cancelButton)
        present(imagePickerActionSheet, animated: true,
                              completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedPhoto = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        activityIndicator.startAnimating()
        dismiss(animated: true, completion: {
            self.performImageRecognition(image: selectedPhoto)
        })
        
    }
    
    func performImageRecognition(image: UIImage) {
        tesseract.pageSegmentationMode = .auto
        tesseract.maximumRecognitionTime = 30.0
        tesseract.image = image.g8_blackAndWhite()
        tesseract.recognize()
        
        textView.text = tesseract.recognizedText

        startedSpeaking = false
        activityIndicator.stopAnimating()
    }
    
    @IBAction func playTextView(_ sender: Any) {
        if startedSpeaking {
            if (synth.isPaused) {
                synth.continueSpeaking()
                (sender as! UIButton).setImage(#imageLiteral(resourceName: "Microphone"), for: .normal)
            } else {
                synth.pauseSpeaking(at: .immediate)
                (sender as! UIButton).setImage(#imageLiteral(resourceName: "MutedMicrophone"), for: .normal)
            }
        } else {
            utter = AVSpeechUtterance(string: textView.text)
            utter.rate = 0.4
            synth.speak(utter)
            
            startedSpeaking = true
        }
    }
}


